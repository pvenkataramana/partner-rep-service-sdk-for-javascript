import PostalAddress from 'postal-object-model';

export default class UpdatePartnerRepPostalAddressReq {

    _id:number;

    _postalAddress:PostalAddress;

    /**
     * @param {number} id
     * @param {PostalAddress} postalAddress
     */
    constructor(id:number,
                postalAddress:PostalAddress) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!postalAddress) {
            throw new TypeError('postalAddress required');
        }
        this._postalAddress = postalAddress;

    }


    get id():number {
        return this._id;
    }

    get postalAddress():PostalAddress {
        return this._postalAddress;
    }

    toJSON() {
        return {
            id: this._id,
            postalAddress: this._postalAddress.toJSON()
        };
    }
}