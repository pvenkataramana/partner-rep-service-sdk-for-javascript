import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import GetPartnerRepBankInfoUpdateUrlReq from './getPartnerRepBankInfoUpdateUrlReq';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class GetPartnerRepBankInfoUpdateUrlFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerRepServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Gets a transient url where a partner reps bank info can be updated
     * @param {GetPartnerRepBankInfoUpdateUrlReq} request
     * @param {string} accessToken
     * @returns {Promise.<string>}
     */
    execute(request:GetPartnerRepBankInfoUpdateUrlReq,
            accessToken:string):Promise<string> {

        return this._httpClient
            .createRequest(`partner-reps/${request.partnerRepId}/bank-info-update-url`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                returnUrl: request.returnUrl
            })
            .send()
            .then(response => response.content);
    }
}

export default GetPartnerRepBankInfoUpdateUrlFeature;