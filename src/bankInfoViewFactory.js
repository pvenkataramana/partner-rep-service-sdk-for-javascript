import BankInfoView from './bankInfoView';

export default class BankInfoViewFactory {

    /**
     * @param {object} data
     * @returns {BankInfoView}
     */
    static construct(data):BankInfoView {

        const id = data.id;

        const isExists = data.isExists;

        return new BankInfoView(
            id,
            isExists
        );

    }

}
