import {PostalAddress} from 'postal-object-model';
import PartnerRepW9Update from '../src/partnerRepW9Update';
import PartnerRepBankInfoUpdate from '../src/partnerRepBankInfoUpdate';

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default {
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '0000000000',
    accountId: '000000000000000000',
    sapVendorNumber: '0000000000',
    userId: '0000000000',
    emailAddress: 'email@test.com',
    url: 'https://dummy-url.com',
    postalAddress: new PostalAddress(
        'street',
        'city',
        'WA',
        'postalCode',
        'US'
    ),
    transactionId: 'transactionId'
};