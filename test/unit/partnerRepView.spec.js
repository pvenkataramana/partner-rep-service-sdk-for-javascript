import PartnerRepView from '../../src/partnerRepView';
import dummy from '../dummy';

/*
 tests
 */
describe('PartnerRepView class', () => {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        null,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.emailAddress,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    expectedId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);
        });
        it('throws if firstName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.userId,
                        null,
                        dummy.lastName,
                        dummy.emailAddress,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'firstName required');
        });
        it('sets firstName', () => {
            /*
             arrange
             */
            const expectedFirstName = dummy.firstName;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.userId,
                    expectedFirstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualFirstName = objectUnderTest.firstName;
            expect(actualFirstName).toEqual(expectedFirstName);
        });
        it('throws if lastName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.userId,
                        dummy.lastName,
                        null,
                        dummy.emailAddress,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'lastName required');
        });
        it('sets lastName', () => {
            /*
             arrange
             */
            const expectedLastName = dummy.lastName;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.userId,
                    dummy.firstName,
                    expectedLastName,
                    dummy.emailAddress,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualLastName = objectUnderTest.lastName;
            expect(actualLastName).toEqual(expectedLastName);
        });
        it('throws if emailAddress is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.userId,
                        dummy.firstName,
                        dummy.lastName,
                        null,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'emailAddress required');
        });
        it('sets emailAddress', () => {
            /*
             arrange
             */
            const expectedEmailAddress = dummy.emailAddress;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.userId,
                    dummy.firstName,
                    dummy.lastName,
                    expectedEmailAddress,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualEmailAddress = objectUnderTest.emailAddress;
            expect(actualEmailAddress).toEqual(expectedEmailAddress);
        });
        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.userId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.emailAddress,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountId required');
        });
        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedAccountId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.userId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    expectedAccountId
                );

            /*
             assert
             */
            const actualAccountId = objectUnderTest.accountId;
            expect(actualAccountId).toEqual(expectedAccountId);
        });
        it('does not throw if postalAddress is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.userId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.emailAddress,
                        dummy.accountId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets postalAddress', () => {
            /*
             arrange
             */
            const expectedPostalAddress = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.userId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    dummy.accountId,
                    expectedPostalAddress
                );

            /*
             assert
             */
            const actualPostalAddress = objectUnderTest.postalAddress;
            expect(actualPostalAddress).toEqual(expectedPostalAddress);
        });
        it('does not throw if phoneNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.userId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.emailAddress,
                        dummy.accountId,
                        dummy.postalAddress,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets phoneNumber', () => {
            /*
             arrange
             */
            const expectedPhoneNumber = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.userId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    dummy.accountId,
                    dummy.postalAddress,
                    expectedPhoneNumber
                );

            /*
             assert
             */
            const actualPhoneNumber = objectUnderTest.phoneNumber;
            expect(actualPhoneNumber).toEqual(expectedPhoneNumber);
        });
        it('does not throw if sapVendorNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.userId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.emailAddress,
                        dummy.accountId,
                        dummy.postalAddress,
                        dummy.phoneNumber,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets sapVendorNumber', () => {
            /*
             arrange
             */
            const expectedSapVendorNumber = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.userId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.emailAddress,
                    dummy.accountId,
                    dummy.postalAddress,
                    dummy.phoneNumber,
                    expectedSapVendorNumber
                );

            /*
             assert
             */
            const actualSapVendorNumber = objectUnderTest.sapVendorNumber;
            expect(actualSapVendorNumber).toEqual(expectedSapVendorNumber);
        });

    });
});
