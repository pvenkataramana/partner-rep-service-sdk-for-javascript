import GetPartnerRepBankInfoUpdateUrlReq from '../../src/getPartnerRepBankInfoUpdateUrlReq';
import dummy from '../dummy';

/*
 tests
 */
describe('GetPartnerRepBankInfoUpdateUrlReq class', () => {
    describe('constructor', () => {
        it('throws if partnerRepId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new GetPartnerRepBankInfoUpdateUrlReq(
                        null,
                        dummy.url
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerRepId required');

        });
        it('sets partnerRepId', () => {
            /*
             arrange
             */
            const expectedPartnerRepId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new GetPartnerRepBankInfoUpdateUrlReq(
                    expectedPartnerRepId,
                    dummy.url
                );

            /*
             assert
             */
            const actualPartnerRepId =
                objectUnderTest.partnerRepId;

            expect(actualPartnerRepId).toEqual(expectedPartnerRepId);

        });
        it('throws if returnUrl is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new GetPartnerRepBankInfoUpdateUrlReq(
                        dummy.userId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'returnUrl required');

        });
        it('sets returnUrl', () => {
            /*
             arrange
             */
            const expectedReturnUrl = dummy.url;

            /*
             act
             */
            const objectUnderTest =
                new GetPartnerRepBankInfoUpdateUrlReq(
                    dummy.userId,
                    expectedReturnUrl
                );

            /*
             assert
             */
            const actualReturnUrl =
                objectUnderTest.returnUrl;

            expect(actualReturnUrl).toEqual(expectedReturnUrl);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new GetPartnerRepBankInfoUpdateUrlReq(
                    dummy.userId,
                    dummy.url
                );

            const expectedObject =
            {
                partnerRepId: objectUnderTest.partnerRepId,
                returnUrl: objectUnderTest.returnUrl
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
